// import logo from './logo.svg';
import './App.css';
import {Routes,Route} from 'react-router-dom';
import LandingPage from './pages/LandingPage';
import RegisterPage from './pages/RegisterPage';
import LoginPage from './pages/LoginPage';
import AboutUsPage from './pages/AboutUsPage';
import OwnerHomepage from './pages/OwnerHomepage';
import RenterHomepage from './pages/RenterHomepage';
import ListProperties from './pages/ListProperties';
import ManageListings from './pages/ManageListings';
import PropertyDetails from './pages/PropertyDetails';
import PropertyDetailsRenter from './pages/PropertyDetailsRenter';
import ComingSoon from './pages/ComingSoon';
function App() {
  return (
    <Routes>
      <Route path='/' element={<LandingPage/>}/>
      <Route path='/login' element={<LoginPage/>}/>
      <Route path='/register' element={<RegisterPage/>}/>
      <Route path='/aboutus' element={<AboutUsPage/>}/>

      {/* Owner Routes */}
      <Route path='/owner' element={<OwnerHomepage/>}/>
      <Route path='/list-properties' element={<ListProperties/>}/>
      <Route path='/manage-listings' element={<ManageListings/>}/>
      <Route path='/propertydetails/:propertyId' element={<PropertyDetails/>}/>
      {/* Renter routes */}
      <Route path='/renter' element={<RenterHomepage/>}/>
      <Route path='/renter/properties/:propertyId' element={<PropertyDetailsRenter/>}/>
      <Route path='/comingsoon' element={<ComingSoon/>}/>
    </Routes>
  );
}

export default App;
