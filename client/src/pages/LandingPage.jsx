import React from 'react';
import './LandingPage.css'; // Import your custom CSS file for styling
import background from '../images/background.jpg'
import { FaInstagram, FaFacebook, } from "react-icons/fa";
import { FaXTwitter,FaLinkedin } from "react-icons/fa6";
import chimtshel from '../images/chimtshel.svg'
import { Link } from 'react-router-dom';
function LandingPage() {
  return (
    <div className="landing-page">
       {/* Navigation bar - Responsive */}
        
       <nav className="navbar navbar-expand-lg navbar-light bg-light  fixed-top">
  <Link to={'#'} className="navbar-brand">
    {/* Replace with your logo or brand name */}
    <img src={chimtshel} alt="Logo" />
  </Link>
  <button
    className="navbar-toggler"
    type="button"
    data-toggle="collapse"
    data-target="#navbarNavAltMarkup"
    aria-controls="navbarNavAltMarkup"
    aria-expanded="false"
    aria-label="Toggle navigation"
  >
    <span className="navbar-toggler-icon"></span>
  </button>
  <div className="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup">
    <div className="navbar-nav">
      <Link to={"/"} className="nav-item nav-link active mr-3">
        Home
      </Link>
      <Link to={"/aboutus"} className="nav-item nav-link mr-3">
        About Us
      </Link>
      <Link to={'/login'} className="nav-item">
        <button className="btn btn-sm btn-primary">Sign In</button>
      </Link>
    </div>
  </div>
</nav>


    {/* Hero section */}
    <section className="hero d-flex align-items-center justify-content-center" style={{ height: '100vh' }}> {/* Increased height */}
    <div className="quote-cta-container d-flex justify-content-center align-items-center" style={{ marginTop: '-20px' }}> {/* Move the container above */}
        <div className="quote-cta border rounded p-4" style={{ background: 'rgba(0, 0, 0, 0.3)', width: '55%' }}> {/* Transparent background with border and sans-serif font */}
        <div className="text-center">
            <h1>The Keys to Your Dream House</h1>
            <p>
            "Discover Your Dream Home for Rent Today!
            Find the Perfect Space to 
            Call Your Own with Our Wide Selection of Rental Properties.!"
            </p>
            <Link to={'/register'} className="btn btn-primary" style={{borderRadius:'20px'}}>
            Explore Now
            </Link>
        </div>
        </div>
    </div>
    </section>


      {/* Find your Next Dream HOuse */}
      <section className="featured-products" style={{ height: '130vh' }}>
      <div className="container" id='landinpagecard'>
        <h2>Find Your Next Dream House</h2>
        <div className="row">
          {/* First Column */}
          <div className="col-md-4">
            <div className="card " id='lpcard' style={{ marginBottom: '20px'
             }}>
              <img className="card-img-top" src="https://i.pinimg.com/originals/11/f2/d9/11f2d9f0258b1bad18f0db5fe7ac515c.jpg" alt="Card image cap"  style={{ height: '200px', objectFit: 'cover' }}/>
              <div className="card-body">
                <h5 className="card-title" id='lpcard-title'>3 BHK</h5>
                <p className="card-text" id='lpcard-text'>
                  Thimphu, Olokha
                </p>
                <Link to={'/register'} className="btn btn-primary">Learn More</Link>
              </div>
            </div>
          </div>
          {/* Second Column */}
          <div className="col-md-4">
            <div className="card text-black" id='lpcard' style={{ marginBottom: '20px' }}>
              <img className="card-img-top" src="https://i.pinimg.com/originals/54/96/12/549612f167093d404d9539062fd2d276.jpg" alt="Card image cap" style={{ height: '200px', objectFit: 'cover' }} />
              <div className="card-body">
              <h5 className="card-title" id='lpcard-title'>2 BHK</h5>
                <p className="card-text" id='lpcard-text'>
                 Paro, Near Dzong
                </p>
                <Link to={'/register'} className="btn btn-primary">Learn More</Link>
              </div>
            </div>
          </div>
          {/* Third Column */}
          <div className="col-md-4">
            <div className="card text-black" id='lpcard' style={{ marginBottom: '20px' }}>
              <img className="card-img-top" src="https://www.architectandinteriorsindia.com/public/styles/portrait/public/images/2020/12/29/kitchen.jpg?K8rUuIkN" alt="Card image cap" style={{ height: '200px', objectFit: 'cover' }} />
              <div className="card-body">
              <h5 className="card-title" id='lpcard-title'>3 BHK</h5>
                <p className="card-text" id='lpcard-text'>
                 Thimphu, Main Town
                </p>
                <Link to={'/register'} className="btn btn-primary">Learn More</Link>
              </div>
            </div>
          </div>
          {/* Fourth Column */}
          <div className="col-md-4">
            <div className="card text-black" id='lpcard' style={{ marginBottom: '20px' }}>
              <img className="card-img-top" src="https://bhutanspiritsanctuary.com/images/gallery/facilities/3.jpg" alt="Card image cap" style={{ height: '200px', objectFit: 'cover' }} />
              <div className="card-body">
              <h5 className="card-title" id='lpcard-title'>4 BHK</h5>
                <p className="card-text" id='lpcard-text'>
                 Punakha, Near Town
                </p>
                <Link to={'register'} className="btn btn-primary">Learn More</Link>
              </div>
            </div>
          </div>
          {/* Fifth Column */}
          <div className="col-md-4">
            <div className="card text-black" id='lpcard' style={{ marginBottom: '20px' }}>
              <img className="card-img-top" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcToW5fyjreOCXFMfApaZr_-WjzuaAT2jXvfdNRbSli2WA&s" alt="Card image cap" style={{ height: '200px', objectFit: 'cover' }} />
              <div className="card-body">
              <h5 className="card-title" id='lpcard-title'>1 BHK</h5>
                <p className="card-text" id='lpcard-text'>
                 Thimphu, Babesa
                </p>
                <Link to={'register'} className="btn btn-primary">Learn More</Link>
              </div>
            </div>
          </div>
          {/* Sixth Column */}
          <div className="col-md-4">
            <div className="card text-black" id='lpcard' style={{ marginBottom: '20px' }}>
              <img className="card-img-top" src="https://media.cntraveler.com/photos/6151d560354d04219ad47eb7/16:9/w_2560,c_limit/COMO%20Bhutan_mm858_Como_Punakha-0994_FINAL.jpg" alt="Card image cap"  style={{ height: '200px', objectFit: 'cover' }}/>
              <div className="card-body">
              <h5 className="card-title" id='lpcard-title'>2 BHK</h5>
                <p className="card-text" id='lpcard-text'>
                  Thimphu, Taba
                </p>
                <Link to={'/register'} className="btn btn-primary">Learn More</Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>


{/* Connect with us */}
<section className="contact-us" style={{ height: '100vh', backgroundImage: `url(${background})`, backgroundSize: 'cover', backgroundPosition: 'center' }}>
  <div className="container">
    <h2 style={{ color: 'white', marginTop: 0 }}>Let's Connect</h2> {/* Heading remains at the top */}
    <div className="contact-info d-flex align-items-center border rounded p-4" style={{ background: 'rgba(0, 0, 0, 0.3)', width: '50%', minWidth: '300px', color: 'white', marginLeft: 'auto', marginRight: 'auto' }}> {/* Transparent background with border, center-aligned, 40% width */}
      <div style={{ fontSize: '30px', textAlign: 'center' }}> {/* Increased font size and centered text */}
        <p>"We believe in the power of connection. Let's chat and see how we can help you achieve your goals."</p>
        {/* Replace with your social media links */}
        <ul className="social-icons list-unstyled d-flex flex-row justify-content-center mb-0"> {/* Flex-row and center the icons */}
          <li className="me-3">
            <Link to={'#'}>
              <FaFacebook size={40} color="white" /> {/* Increased icon size and set color to white */}
            </Link>
          </li>
          <li className="me-3">
            <Link to={'#'}>
              <FaXTwitter size={40} color="white" /> {/* Increased icon size and set color to white */}
            </Link>
          </li>
          <li className="me-3">
            <Link to={'#'}>
                <FaInstagram size={40} color="white" /> {/* Increased icon size and set color to white */}
            </Link>
          </li>
          <li className="me-3">
            <Link to={'#'}>
                <FaLinkedin size={40} color="white" /> {/* Increased icon size and set color to white */}
            </Link>
          </li>
          {/* Add more social media icons as needed */}
        </ul>
      </div>
    </div>
  </div>
</section>




      {/* Footer */}
      <footer className="footer">
        <div className="container">
          <p>&copy; {new Date().getFullYear()} CHIM TSHEL</p>
        </div>
      </footer>
    </div>
  );
}

export default LandingPage;
