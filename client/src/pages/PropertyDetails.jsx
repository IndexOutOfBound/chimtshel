import React, { useState, useEffect } from 'react';
import { useParams, useNavigate, Link } from 'react-router-dom';
import axios from 'axios';
import { Carousel } from 'react-bootstrap'; // Assuming Bootstrap Carousel is installed
import './LandingPage.css';
import ownerbackground from '../images/houserental.jpg';

const API_BASE_URL = 'https://chimtshel.onrender.com'; // Replace with your backend API URL

const PropertyDetails = () => {
  const { propertyId } = useParams();
//   const navigate = useNavigate();
  const [listing, setListing] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchListingDetails = async () => {
      try {
        const response = await axios.get(`${API_BASE_URL}/properties/${propertyId}`);
        setListing(response.data.listing);
      } catch (error) {
        console.error('Error fetching listing details:', error);
        setError(error.message);
      } finally {
        setIsLoading(false);
      }
    };

    fetchListingDetails();
  }, [propertyId]);

  if (isLoading) {
    return <div className="text-center mt-5">Loading...</div>;
  }

  if (error) {
    return (
      <div className="alert alert-danger mt-5" role="alert">
        Error: {error}
      </div>
    );
  }

  const {
    property_name,
    address,
    description,
    rent,
    view_schedule,
    is_active,
    image_paths,
    image_descriptions,
  } = listing;

  const formattedRent = rent; // Format rent to 2 decimal places

  return (
    <div className="landing-page" style={{color:'white', height:'100vh' , backgroundImage: `url(${ownerbackground})`, backgroundSize: 'cover', backgroundPosition: 'center'}}>
      <div className="container mt-5">
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <Link to="/manage-listings" style={{ textDecoration: 'none' }}>Home</Link>
            </li>
            <li className="breadcrumb-item active text-white" aria-current="page">
              {property_name}
            </li>
          </ol>
        </nav>

        <h1 className="text-center">{property_name}</h1>
      <div className="d-flex justify-content-between align-items-center mb-4">
        <span>
          {is_active ? (
            <span className="badge bg-success">Active</span>
          ) : (
            <span className="badge bg-secondary">Inactive</span>
          )}
        </span>
        {view_schedule && (
          <span className="text-muted">Viewing Schedule: {view_schedule}</span>
        )}
      </div>

      {/* Image Carousel */}
      <Carousel fade>
        {image_paths.map((imagePath, index) => (
          <Carousel.Item key={index}>
            <img
              className="d-block w-100 img-fluid"
              src={`${API_BASE_URL}/uploads/${imagePath}`} // Assuming image paths are relative to API URL
              alt={image_descriptions[index] || `Image ${index + 1}`}
            />
            <Carousel.Caption>
              <h3>{image_descriptions[index] || `Image ${index + 1}`}</h3>
            </Carousel.Caption>
          </Carousel.Item>
        ))}
      </Carousel>

      <div className="mt-4">
        <h3>Property Details</h3>
        <h5>{description}</h5>
        <div className="d-flex justify-content-between mt-4">
          <h5 className="fw-bold">Rent:</h5>
          <h5>Nu{formattedRent}</h5>
        </div>
        <div className="d-flex justify-content-between mt-2 mb-5">
          <h5 className="fw-bold">Address:</h5>
          <h5>{address}</h5>
        </div>
      </div>
      </div>
    </div>
  );
};

export default PropertyDetails;
