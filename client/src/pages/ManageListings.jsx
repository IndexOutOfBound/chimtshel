import React, { useState, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';
import logo from '../images/chimtshel.svg';
import './LandingPage.css'; // Assuming styles are defined here
import ownerbackground from '../images/houserental.jpg';
const API_BASE_URL = 'https://chimtshel.onrender.com';

const ManageListings = () => {
  const navigate = useNavigate();
  const getUserData = sessionStorage.getItem('userdata');
  let name = '';
  let email = '';
  let userdata = null;
  if (!getUserData) {
    navigate('/');
  } else {
    userdata = JSON.parse(getUserData);
    email = userdata.email;
    name = userdata.name;
  }

  const [listings, setListings] = useState([]);
  const [alertMessage, setAlertMessage] = useState('');

  useEffect(() => {
    fetchListings();
  }, []);

  const fetchListings = async () => {
    try {
      const response = await axios.get(`${API_BASE_URL}/owner/listings`, {
        params: { owner_email: email },
      });
      setListings(response.data.listings);
      console.log(listings);
    } catch (error) {
      console.error('Error fetching listings:', error);
      showAlert('Error fetching listings', 'danger');
    }
  };

  const deactivateListing = async (id) => {
    try {
      await axios.put(`${API_BASE_URL}/owner/listings/${id}/deactivate`, {
        owner_email: email,
      });
      fetchListings();
      showAlert('Listing deactivated successfully', 'success');
    } catch (error) {
      console.error('Error deactivating listing:', error);
      showAlert('Error deactivating listing', 'danger');
    }
  };

  const deleteListing = async (id) => {
    try {
      await axios.delete(`${API_BASE_URL}/owner/listings/${id}`, {
        data: { owner_email: email },
      });
      fetchListings();
      showAlert('Listing deleted successfully', 'success');
    } catch (error) {
      console.error('Error deleting listing:', error);
      showAlert('Error deleting listing', 'danger');
    }
  };

  const showAlert = (message, type) => {
    setAlertMessage({ message, type });
    setTimeout(() => {
      setAlertMessage('');
    }, 3000); // Hide the alert after 3 seconds
  };

  return (
    <div className="landing-page"  style={{height:'100vh' , backgroundImage: `url(${ownerbackground})`, backgroundSize: 'cover', backgroundPosition: 'center'}}>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <div className="container-fluid">
            <Link to="/owner" className="navbar-brand">
              <img src={logo} alt="Logo" className="logo" />
              {/* Display your logo */}
            </Link>
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarNav"
              aria-controls="navbarNav"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
              <ul className="navbar-nav ms-auto">
                {/* Align navigation to the left */}
                <li className="nav-item">
                  <Link to="/list-properties" className="nav-link">
                    List Properties
                  </Link>
                </li>
                <li className="nav-item">
                  <Link to="/manage-listings" className="nav-link">
                    Manage Listings
                  </Link>
                </li>
                <li className="nav-item">
                  <Link to="/comingsoon" className="nav-link">
                    Schedule Viewings
                  </Link>
                </li>
                <li className="nav-item">
                  <Link to="/login" className="nav-link">
                    Logout
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>

        <h1 className="text-center text-white mt-4">Manage Listings</h1>

        {alertMessage && (
          <div
            className={`alert alert-${alertMessage.type} position-fixed bottom-0 end-0 m-3`}
            role="alert"
          >
            {alertMessage.message}
          </div>
        )}

        <div className="listings-container mt-4">
          <table className="w-100 table-hover table-bordered bg-transparent text-white rounded-sm">
            <thead className="table-primary"> {/* Added table-primary for highlighting */}
              <tr className='text-white'>
                <th scope="col">Property Name</th>
                <th scope="col">Address</th>
                <th scope="col">Rent</th>
                <th scope="col">Status</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
              {listings.map((listing) => (
                <tr key={listing.property_id}>
                  <td>{listing.property_name}</td>
                  <td>{listing.address}</td>
                  <td>Nu.{listing.rent}</td>
                  <td>{listing.is_active ? 'Active' : 'Not Active'}</td>
                  <td className="d-flex justify-content-between align-items-center">
                    <Link
                      to={`/propertydetails/${listing.property_id}`}
                      className="btn btn-primary btn-sm"
                    >
                      View Details
                    </Link>
                    <button
                      onClick={() => deactivateListing(listing.property_id)}
                      className="btn btn-danger btn-sm"
                    >
                      Deactivate
                    </button>
                    <button
                      onClick={() => deleteListing(listing.property_id)}
                      className="btn btn-warning btn-sm"
                    >
                      Delete
                    </button>
                    <Link
                      to={`/edit-listing/${listing.property_id}`}
                      className="btn btn-secondary btn-sm"
                    >
                      Edit
                    </Link>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
  );
};

export default ManageListings;
