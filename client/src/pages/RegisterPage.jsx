import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import './LandingPage.css';
import chimtshel from '../images/chimtshel.svg';
import backgroundImage from '../images/background.jpg';

const API_BASE_URL = 'https://chimtshel.onrender.com';

function RegisterPage() {
  const [formData, setFormData] = useState({
    name: '',
    phoneNumber: '',
    email: '',
    password: '',
    gender: '',
    currentAddress: '',
    typeOfUser: ''
  });

  const [alertMessage, setAlertMessage] = useState(null);

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post(`${API_BASE_URL}/register`, formData);
      setAlertMessage({ type: 'success', message: response.data.message });
    } catch (error) {
      console.error('Error:', error);
      if (error.response && error.response.status === 400) {
        setAlertMessage({ type: 'error', message: error.response.data.message });
      } else {
        setAlertMessage({ type: 'error', message: 'An error occurred while processing your request.' });
      }
    }
  };

  return (
    <div className='landing-page' style={{ backgroundImage: `url(${backgroundImage})`, backgroundSize: 'cover', backgroundPosition: 'center'}}>
      {/* Navbar */}
      <nav className="navbar navbar-expand-lg navbar-light bg-light fixed-top">
        <Link to={'#'} className="navbar-brand">
          <img src={chimtshel} alt="Logo" />
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNavAltMarkup"
          aria-controls="navbarNavAltMarkup"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup">
          <div className="navbar-nav">
            <Link to={'/'} className="nav-item nav-link active mr-3">
              Home
            </Link>
            <Link to={'/aboutus'} className="nav-item nav-link mr-3">
              About Us
            </Link>
            <Link to={'/login'} className="nav-item">
              <button className="btn btn-sm btn-primary">Sign In</button>
            </Link>
          </div>
        </div>
      </nav>

      {/* Register Section */}
      <section className="register-section" style={{ minHeight: '100vh' }}>
        <div className="container py-5">
          <div className="row justify-content-center">
            <div className="col-lg-5 mt-5">
              <div className="p-4" style={{ background: 'rgba(0, 0, 0, 0.5)', borderRadius: '10px', border: '1px solid #ccc' }}>
                <h2 className="text-white mb-4">Register</h2>
                <form onSubmit={handleSubmit}>
                  <div className="mb-3">
                    <label htmlFor="name" className="form-label text-white">Name</label>
                    <input type="text" className="form-control" id="name" name="name" value={formData.name} onChange={handleChange} required />
                  </div>
                  <div className="mb-3">
                    <label htmlFor="phoneNumber" className="form-label text-white">Phone number</label>
                    <input type="text" className="form-control" id="phoneNumber" name="phoneNumber" value={formData.phoneNumber} onChange={handleChange} required />
                  </div>
                  <div className="mb-3">
                    <label htmlFor="email" className="form-label text-white">Email ID</label>
                    <input type="email" className="form-control" id="email" name="email" value={formData.email} onChange={handleChange} required />
                  </div>
                  <div className="mb-3">
                    <label htmlFor="password" className="form-label text-white">Password</label>
                    <input type="password" className="form-control" id="password" name="password" value={formData.password} onChange={handleChange} required />
                  </div>
                  <div className="mb-3">
                    <label htmlFor="gender" className="form-label text-white">Gender</label>
                    <select className="form-select" id="gender" name="gender" value={formData.gender} onChange={handleChange} required>
                      <option value="">Select Gender</option>
                      <option value="male">Male</option>
                      <option value="female">Female</option>
                      <option value="other">Other</option>
                    </select>
                  </div>
                  <div className="mb-3">
                    <label htmlFor="currentAddress" className="form-label text-white">Current address</label>
                    <textarea className="form-control" id="currentAddress" name="currentAddress" value={formData.currentAddress} onChange={handleChange} required></textarea>
                  </div>
                  <div className="mb-3">
                    <label htmlFor="typeOfUser" className="form-label text-white">Type of User</label>
                    <select className="form-select" id="typeOfUser" name="typeOfUser" value={formData.typeOfUser} onChange={handleChange} required>
                      <option value="">Select Type of User</option>
                      <option value="renter">Renter</option>
                      <option value="owner">Owner</option>
                    </select>
                  </div>
                  <button type="submit" className="btn btn-primary">Register</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>
       {/* Alert Box */}
       {alertMessage && (
        <div className={`alert alert-${alertMessage.type} alert-dismissible fade show`} role="alert" style={{ position: 'fixed', bottom: '20px', right: '20px', zIndex: '9999' }}>
          {alertMessage.message}
          <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close" onClick={() => setAlertMessage(null)}></button>
        </div>
      )}
    </div>
  );
}

export default RegisterPage;
