import React from 'react';
import { Link,useNavigate } from 'react-router-dom';
import logo from '../images/chimtshel.svg'; // Import your logo image
import './LandingPage.css';
import ownerbackground from '../images/houserental.jpg';

function OwnerHomepage() {
    const navigate = useNavigate();
    const getUserData=sessionStorage.getItem('userdata');
    let name='';
    let email='';
    let userdata=null;
    if (!getUserData) {
      navigate('/')
    } else {
      userdata = JSON.parse(getUserData);
        email = userdata.email;
        name = userdata.name;
    }

  return (
    <div className="landing-page"  style={{height:'100vh' , backgroundImage: `url(${ownerbackground})`, backgroundSize: 'cover', backgroundPosition: 'center'}}>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
          <Link to="/owner" className="navbar-brand">
            <img src={logo} alt="Logo" className="logo" /> {/* Display your logo */}
          </Link>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav ms-auto"> {/* Align navigation to the left */}
              <li className="nav-item">
                <Link to="/list-properties" className="nav-link">List Properties</Link>
              </li>
              <li className="nav-item">
                <Link to="/manage-listings" className="nav-link">Manage Listings</Link>
              </li>
              <li className="nav-item">
                <Link to="/comingsoon" className="nav-link">Schedule Viewings</Link>
              </li>
              <li className="nav-item">
              <Link to="/login" className="nav-link">
                    Logout
                  </Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <div className="container mt-4">
        <div className="row">
          <div className="col-lg-8" style={{color:'white'}}>
            <h1>Welcome back, {name}!</h1>
            <p>Here you can manage your rental properties and agreements.</p>
            {/* Your Properties section */}
            <div className="card" >
              <div className="card-body">
                <h5 className="card-title">Your Properties</h5>
                <p className="card-text">You currently have 5 properties listed for rent.</p>
                <Link to="/list-properties" className="btn btn-primary">View Properties</Link>
              </div>
            </div>
            {/* Upcoming Viewings section */}
            <div className="card mt-3">
              <div className="card-body">
                <h5 className="card-title">Upcoming Viewings</h5>
                <p className="card-text">You have 2 viewings scheduled for this week.</p>
                <Link to="/schedule-viewings" className="btn btn-primary">Schedule Viewings</Link>
              </div>
            </div>
          </div>
          <div className="col-lg-4">
            {/* Additional content */}
            {/* <div className="card mt-3">
              <div className="card-body">
                <h5 className="card-title">Latest Rental Agreements</h5>
                <p className="card-text">You recently signed a new rental agreement. View details.</p>
                <Link to="/manage-listings" className="btn btn-primary">View Agreements</Link>
              </div>
            </div>
            <div className="card mt-3">
              <div className="card-body">
                <h5 className="card-title">Profile Settings</h5>
                <p className="card-text">Update your profile information and preferences.</p>
                <Link to="/profile-settings" className="btn btn-primary">Edit Profile</Link>
              </div>
            </div> */}
          </div>
        </div>
      </div>
    </div>
  );
}

export default OwnerHomepage;
