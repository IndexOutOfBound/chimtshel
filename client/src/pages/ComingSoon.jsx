import React from 'react';
import { Container, Row, Col, Card } from 'react-bootstrap';

const ComingSoon = () => {
  return (
    <Container className="d-flex justify-content-center align-items-center" style={{ height: '100vh' }}>
      <Row>
        <Col>
          <Card className="text-center">
            <Card.Body>
              <Card.Title className="display-4" style={{color:'black'}}>Exciting Features Ahead!</Card.Title>
              <Card.Text className="lead" style={{color:'black'}}>
                We are working hard to bring you this feature. Stay tuned for updates!
              </Card.Text>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

export default ComingSoon;
