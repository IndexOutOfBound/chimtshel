import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import './LandingPage.css';
import logo from '../images/chimtshel.svg';
import axios from 'axios';
import ownerbackground from '../images/houserental.jpg';
const API_BASE_URL = 'https://chimtshel.onrender.com';

function ListProperties() {
    const navigate = useNavigate();
    const getUserData = sessionStorage.getItem('userdata');
    let name = '';
    let email = '';
    let userdata = null;
    if (!getUserData) {
        navigate('/');
    } else {
        userdata = JSON.parse(getUserData);
        email = userdata.email;
        name = userdata.name;
    }
    const [propertyDetails, setPropertyDetails] = useState({
        propertyName: '',
        rent: 0,
        address: '',
        description: '',
        images: [],
        imageDescriptions: [],
    });
    const [message, setMessage] = useState('');
    const [imgs, setImgs] = useState([]);
    const handleChange = (e) => {
        const { name, value } = e.target;
        setPropertyDetails({
            ...propertyDetails,
            [name]: value,
        });
    };

    const handleImageChange = (e) => {
        const files = Array.from(e.target.files);
        console.log("files", files)

        setImgs([...imgs ,URL.createObjectURL(files[0])]);
        setPropertyDetails({
            ...propertyDetails,
            images: [...propertyDetails.images, ...files],
        });
    };
    
    const handleDescriptionChange = (index, e) => {
        const { value } = e.target;
        const imageDescriptions = [...propertyDetails.imageDescriptions];
        imageDescriptions[index] = value;
        setPropertyDetails({
            ...propertyDetails,
            imageDescriptions,
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        
        try {
            const formData = new FormData();
            formData.append('owner_email', email);
            formData.append('propertyName', propertyDetails.propertyName);
            formData.append('address', propertyDetails.address);
            formData.append('description', propertyDetails.description);
            formData.append('rent', propertyDetails.rent);
            
            // Append each image file along with its description
            propertyDetails.images.forEach((image, index) => {
                formData.append('images', image); // Append image file
                formData.append('imageDescriptions', propertyDetails.imageDescriptions[index]); // Append image description
            });
    
            // Log FormData content
            console.log([...formData]);
            console.log(formData)
    
            const response = await axios.post(`${API_BASE_URL}/owner/listproperties`, formData);
            const data = response.data;
    
            // Display message based on the response
            setMessage(data.message);
        } catch (error) {
            console.error('Error:', error);
            setMessage('Error listing property');
        }
    };

    return (
        <div className="landing-page"  style={{color:'white', height:'100vh' , backgroundImage: `url(${ownerbackground})`, backgroundSize: 'cover', backgroundPosition: 'center'}}>
                <nav className="navbar navbar-expand-lg navbar-light bg-light ">
                    <div className="container-fluid">
                        <Link to="/owner" className="navbar-brand">
                            <img src={logo} alt="Logo" className="logo" />
                            {/* Display your logo */}
                        </Link>
                        <button
                            className="navbar-toggler"
                            type="button"
                            data-bs-toggle="collapse"
                            data-bs-target="#navbarNav"
                            aria-controls="navbarNav"
                            aria-expanded="false"
                            aria-label="Toggle navigation"
                        >
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarNav">
                            <ul className="navbar-nav ms-auto">
                                {/* Align navigation to the left */}
                                <li className="nav-item">
                                    <Link to="/list-properties" className="nav-link">
                                        List Properties
                                    </Link>
                                </li>
                                <li className="nav-item">
                                    <Link to="/manage-listings" className="nav-link">
                                        Manage Listings
                                    </Link>
                                </li>
                                <li className="nav-item">
                                    <Link to="/schedule-viewings" className="nav-link">
                                        Schedule Viewings
                                    </Link>
                                </li>
                                <li className="nav-item">
                                <Link to="/login" className="nav-link">
                                    Logout
                                </Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <div className="row justify-content-center">
                    <div className="col-lg-6">
                        <h1>List Your Property</h1>
                        <form onSubmit={handleSubmit}>
                            <div className="mb-3">
                                <label htmlFor="propertyName" className="form-label">
                                    Property Name
                                </label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="propertyName"
                                    name="propertyName"
                                    value={propertyDetails.propertyName}
                                    onChange={handleChange}
                                    required
                                />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="address" className="form-label">
                                    Address
                                </label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="address"
                                    name="address"
                                    value={propertyDetails.address}
                                    onChange={handleChange}
                                    required
                                />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="rent" className="form-label">
                                    Rent
                                </label>
                                <input
                                    type="number"
                                    className="form-control"
                                    id="rent"
                                    name="rent"
                                    value={propertyDetails.rent}
                                    onChange={handleChange}
                                    required
                                />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="description" className="form-label">
                                    Description
                                </label>
                                <textarea
                                    className="form-control"
                                    id="description"
                                    name="description"
                                    value={propertyDetails.description}
                                    onChange={handleChange}
                                    required
                                />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="images" className="form-label">
                                    Upload Images
                                </label>
                                <input
                                    type="file"
                                    className="form-control"
                                    id="images"
                                    name="images"
                                    accept="image/*"
                                    multiple
                                    onChange={handleImageChange}
                                    required
                                />
                            </div>
                            {imgs.map((image, index) => (
                                <div key={index} className="mb-3">
                                    <label htmlFor={`imageDescription${index}`} className="form-label">
                                        Description for Image {index + 1}
                                    </label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        id={`imageDescription${index}`}
                                        name={`imageDescription${index}`}
                                        value={propertyDetails.imageDescriptions[index] || ''}
                                        onChange={(e) => handleDescriptionChange(index, e)}
                                        required
                                    />
                                    <img
                                        src={image}
                                        alt={`Image ${index + 1}`}
                                        className="uploaded-image"
                                        style={{ maxWidth: '400px', maxHeight: '300px' }}
                                    />
                                </div>
                            ))}
                            <button type="submit" className="btn btn-primary">
                                List Property
                            </button>
                        </form>
                        {message && (
                            <div className="alert alert-success alert-dismissible fade show position-fixed bottom-0 end-0" role="alert">
                                {message}
                                <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                        )}
                    </div>
                    <div className="col-lg-4">
                        <div className="card property-preview">
                            <h5 className="card-header">Property Preview</h5>
                            <div className="card-body">
                                <h5 className="card-title">{propertyDetails.propertyName}</h5>
                                <p className="card-text">{propertyDetails.address}</p>
                                <p className="card-text">{propertyDetails.description}</p>
                                {imgs.map((image, index) => (
                                    <div key={index}>
                                        <img src={image} alt={`Image ${index + 1}`} className="preview-image" />
                                        <p>{propertyDetails.imageDescriptions[index]}</p>
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
    );
}

export default ListProperties;

