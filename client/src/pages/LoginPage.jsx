import React,{useState} from 'react';
import { Link,useNavigate } from 'react-router-dom';
import './LandingPage.css';
import chimtshel from '../images/chimtshel.svg';
import backgroundImage from '../images/background.jpg'; // Assuming you have a background image
import axios from 'axios';
const API_BASE_URL = 'https://chimtshel.onrender.com';
//const API_BASE_URL = 'http://localhost:5000'
function LoginPage() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post( `${API_BASE_URL}/login`, { email, password });
      console.log(response.data)
      if (response.status === 200) {
        const userdata = {
          email: email,
          name: response.data.name,
        };
        console.log(userdata)
        sessionStorage.setItem('userdata', JSON.stringify(userdata));
        if (response.data.role === 'owner') {
          navigate('/owner');
        } else if (response.data.role === 'renter') {
          navigate('/renter');
        }
      }
    } catch (error) {
      console.error('Error:', error);
      if (error.response) {
        alert(error.response.data.message); // Display error message if login fails
      } else {
        alert('Something went wrong. Please try again.');
      }
    }
  };
  return (
    <div className='landing-page'  style={{ backgroundImage: `url(${backgroundImage})`, backgroundSize: 'cover', backgroundPosition: 'center'}}>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <Link to={'#'} className="navbar-brand">
          {/* Replace with your logo or brand name */}
          <img src={chimtshel} alt="Logo" />
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNavAltMarkup"
          aria-controls="navbarNavAltMarkup"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup">
          <div className="navbar-nav">
            <Link to={'/'} className="nav-item nav-link  mr-3">
              Home
            </Link>
            <Link to={'/aboutus'} className="nav-item nav-link mr-3">
              About Us
            </Link>
            <Link to={'/login'} className="nav-item active">
              <button className="btn btn-sm btn-primary">Sign In</button>
            </Link>
          </div>
        </div>
      </nav>

      <section className="login-section "style={{height:'87vh' }}>
        <div className="container py-5">
          <div className="d-flex justify-content-center align-items-center" style={{ width: '45%', margin: 'auto', background: 'rgba(0, 0, 0, 0.5)', borderRadius: '10px', border: '1px solid #ccc' }}>
            <div className="p-4">
              <h2 className="text-white">Login</h2>
              <form>
                <div className="mb-3">
                  <label htmlFor="email" className="form-label text-white">Email address</label>
                  <input type="email" className="form-control" id="email" onChange={(e)=> setEmail(e.target.value)} />
                </div>
                <div className="mb-3">
                  <label htmlFor="password" className="form-label text-white">Password</label>
                  <input type="password" className="form-control" id="password" onChange={(e)=> setPassword(e.target.value)}/>
                </div>
                <button type="submit" className="btn btn-primary" onClick={handleSubmit}>Login</button>
                <div className="mt-3">
                  <Link to={'/forgot-password'} className="text-white">Forgot Password?</Link>
                </div>
                <div className="mt-3">
                  <p className="text-white">Don't have an account? <Link to={'/register'} className="text-white">Register</Link></p>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

export default LoginPage;
