import React, { useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { Button, Dropdown, InputGroup, Card } from 'react-bootstrap';
import logo from '../images/chimtshelwhite.svg'
import axios from 'axios';
const API_BASE_URL = 'https://chimtshel.onrender.com';

function RenterHomepage() {
  const navigate = useNavigate();
  const getUserData = sessionStorage.getItem('userdata');
  let name = '';
  let email = '';
  let userdata = null;
  if (!getUserData) {
    navigate('/');
  } else {
    userdata = JSON.parse(getUserData);
    email = userdata.email;
    name = userdata.name;
  }

  const [listings, setListings] = useState([]);
  const [searchText, setSearchText] = useState('');
  const [selectedRentRange, setSelectedRentRange] = useState('All');
  const [selectedLocation, setSelectedLocation] = useState('All');
  const [noResults, setNoResults] = useState(false);

  useEffect(() => {
    fetchListings();
  }, []);

  const fetchListings = async () => {
    try {
      const response = await axios.get(`${API_BASE_URL}/renter/listings`);
      setListings(response.data);
    } catch (e) {
      console.log(e);
    }
  };
  const rentRangeOptions = [
    { label: 'All', min: 0, max: Infinity },
    { label: 'Nu.3000 - Nu.6000', min: 3000, max: 6000 },
    { label: 'Nu.6000 - Nu.9000', min: 6000, max: 9000 },
    { label: 'Nu.9000 - Nu.12000', min: 9000, max: 12000 }
  ];

  const isInRentRange = (rent, range) => {
    const [min, max] = range.split(' - ').map(val => parseInt(val.replace('Nu.', '').trim()));
    return parseInt(rent) >= min && parseInt(rent) <= max;
  };

  const filteredListings = listings.filter(listing =>
    (listing.property_name.toLowerCase().includes(searchText.toLowerCase()) ||
    listing.address.toLowerCase().includes(searchText.toLowerCase())) &&
    (selectedRentRange === 'All' || isInRentRange(listing.rent, selectedRentRange))
  );
  

  const noSearchResults = searchText && filteredListings.length === 0;
  const noDropdownResults = selectedRentRange !== 'All' && filteredListings.length === 0;

  return (
    <div className="bg-dark py-8 px-4 md:px-6">
      <div className="container">
        <div className="d-flex justify-content-between mb-6">
        <Link to="/renter" className="navbar-brand mt-3 mb-4">
                            <img src={logo} alt="Logo" className="logo" />
                            {/* Display your logo */}
                        </Link>
          <div className="d-flex gap-4 mt-4">
            <Dropdown >
              <Dropdown.Toggle variant="outline-secondary" id="rentRangeDropdown" style={{color:'white', opacity:1}}>
                Rent Range
              </Dropdown.Toggle>
              <Dropdown.Menu>
                {rentRangeOptions.map(option => (
                 <Dropdown.Item key={option.label} onClick={() => setSelectedRentRange(option.label)}>
                 {option.label}
               </Dropdown.Item>
                ))}
              </Dropdown.Menu>
            </Dropdown>
            <Link to="/login" className="nav-link" style={{color:'white'}}>
                    Logout
                  </Link>
          </div>
        </div>
        <div className="relative">
          <InputGroup className="mb-3">
            <InputGroup.Text id="basic-addon1">
              <SearchIcon />
            </InputGroup.Text>
            <input
              type="text"
              className="form-control"
              placeholder="Search for a home..."
              aria-label="Search for a home..."
              aria-describedby="basic-addon1"
              value={searchText}
              onChange={(e) => setSearchText(e.target.value)}
            />
          </InputGroup>
        </div>
      </div>

      {/* Listing Cards */}
      {noSearchResults || noDropdownResults ? (
        <div className="container">
          <p className="text-light">No results found.</p>
        </div>
      ) : (
        <div className="row">
          {filteredListings.map(listing => (
            <div className="col-md-4" key={listing.property_id}>
              <Card style={{ marginBottom: '20px' }}>
                <Card.Img variant="top" src={`${API_BASE_URL}/uploads/${listing.image_paths[0]}`} style={{ height: '200px', objectFit: 'cover' }} />
                <Card.Body>
                  <Card.Title>{listing.property_name}</Card.Title>
                  <Card.Text style={{ opacity: 0.6 }}>{listing.address}</Card.Text>
                  <Card.Text>{listing.rent}/Month</Card.Text>
                  <Link to={`/renter/properties/${listing.property_id}`} className="btn btn-primary">View Deals</Link>
                </Card.Body>
              </Card>
            </div>
          ))}
        </div>
      )}
    </div>
  );
}

function SearchIcon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    >
      <circle cx="11" cy="11" r="8" />
      <path d="m21 21-4.3-4.3" />
    </svg>
  );
}

export default RenterHomepage;
