import React from 'react';
import { Link } from 'react-router-dom';
import './LandingPage.css';
import chimtshel from '../images/chimtshel.svg';
import backgroundImage from '../images/background.jpg'; // Add your background image path here

function AboutUsPage() {
  return (
    <div className='landing-page' style={{ backgroundImage: `url(${backgroundImage})`, backgroundSize: 'cover', backgroundPosition: 'center', minHeight: '100vh' }}>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <Link to={'#'} className="navbar-brand">
          {/* Replace with your logo or brand name */}
          <img src={chimtshel} alt="Logo" />
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNavAltMarkup"
          aria-controls="navbarNavAltMarkup"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup">
          <div className="navbar-nav">
            <Link to={'/'} className="nav-item nav-link mr-3">
              Home
            </Link>
            <Link to={'/aboutus'} className="nav-item active nav-link mr-3">
              About Us
            </Link>
            <Link to={'/login'} className="nav-item">
              <button className="btn btn-sm btn-primary">Sign In</button>
            </Link>
          </div>
        </div>
      </nav>
      <div className="container d-flex align-items-center justify-content-center">
        <div className="col-4">
          <div className="row d-flex flex-column justify-content-center mt-5">
            {/* Aim section */}
            <div className="aim border rounded p-4 mb-4" style={{ backgroundColor: 'rgba(0, 0, 0, 0.3)', color: 'white', fontSize: '1.2rem', transition: 'background-color 0.3s', ':hover': { backgroundColor: 'rgba(255, 255, 255, 0.3)' } }}>
              <h2 className="mb-3">Our Aim</h2>
              <p>
                To simplify the house hunting process for individuals relocating within Bhutan by providing a user-friendly online platform.
              </p>
            </div>
          </div>
          <div className="row d-flex flex-column justify-content-center mt-4">
            {/* Objective section */}
            <div className="objective border rounded p-4" style={{ backgroundColor: 'rgba(0, 0, 0, 0.3)', color: 'white', fontSize: '1.2rem', transition: 'background-color 0.3s', ':hover': { backgroundColor: 'rgba(255, 255, 255, 0.3)' } }}>
              <h2 className="mb-3">Our Objective</h2>
              <p>
                To develop an efficient system that helps users find vacant and affordable housing units in their new locations within Bhutan.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AboutUsPage;
