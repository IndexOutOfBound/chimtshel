import React, { useState, useEffect } from 'react';
import {FaArrowLeft} from 'react-icons/fa'
import { useParams , Link} from 'react-router-dom';
import axios from 'axios';
import { Button } from 'react-bootstrap';

const API_BASE_URL = 'https://chimtshel.onrender.com';

function PropertyDetailsRenter() {
  const { propertyId } = useParams();
  const [listing, setListing] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchListingDetails = async () => {
      try {
        const response = await axios.get(`${API_BASE_URL}/renter/properties/${propertyId}`);
        setListing(response.data.property);
      } catch (error) {
        console.error('Error fetching listing details:', error);
        setError(error.message);
      } finally {
        setIsLoading(false);
      }
    };

    fetchListingDetails();
  }, [propertyId]);

  if (isLoading) {
    return <div className="text-center mt-5">Loading...</div>;
  }

  if (error) {
    return (
      <div className="alert alert-danger mt-5" role="alert">
        Error: {error}
      </div>
    );
  }

  if (!listing) {
    return (
      <div className="alert alert-warning mt-5" role="alert">
        No listing found for property ID: {propertyId}
      </div>
    );
  }

  const {
    property_name,
    address,
    description,
    rent,
    owner_name,
    owner_phonenumber,
    image_paths,
    image_descriptions,
  } = listing;

  const formattedRent = rent;

  return (
    <div className="bg-dark py-8 px-4 md:px-6">
        <Link to={'/renter'} className='mt-5' style={{textDecoration:'none'}}>
            <FaArrowLeft size={30} color='white' style={{marginTop:15}}/>
        </Link>
      <div className="max-w-6xl mx-auto">
        <div className="row">
          <div className='col-6'>
            <div className="row mt-5">
              {image_paths.slice(0, Math.ceil(image_paths.length / 2)).map((image, index) => (
                <div key={index}>
                  <img
                    alt={`Property Image ${index + 1}`}
                    className="img-fluid" 
                    style={{borderRadius:'10px'}}
                    height={400}
                    src={`${API_BASE_URL}/uploads/${image}`}
                    width={600}
                  />
                  <p className="text-white mt-2">{image_descriptions[index]}</p>
                </div>
              ))}
            </div>
            <div className="grid gap-4">
              {image_paths.slice(Math.ceil(image_paths.length / 2)).map((image, index) => (
                <div key={index}>
                  <img
                    alt={`Property Image ${Math.ceil(image_paths.length / 2) + index + 1}`}
                    className="img-fluid" 
                    style={{borderRadius:'10px'}}
                    height={400}
                    src={`${API_BASE_URL}/uploads/${image}`}
                    width={600}
                  />
                  <p className="text-white mt-2">{image_descriptions[Math.ceil(image_paths.length / 2) + index]}</p>
                </div>
              ))}
            </div>
          </div>
          <div className='col-6 '>
            <div className="row mt-5">
              <div className="col-12">
                <h1 className="text-white">{property_name}</h1>
                <p className="text-white">{address}</p>
              </div>
            </div>
            <div className="row">
              <div className="col-8">
                <p className="text-white h4">Nu.{formattedRent}/month</p>
              </div>
              <div className="col-4">
                <div className="d-flex align-items-center">
                  <div className="rounded-circle bg-light d-flex align-items-center justify-content-center" style={{width: '40px', height: '40px'}}>
                    JD
                  </div>
                  <div className="ms-2">
                    <p className="text-white mb-0">{owner_name}</p>
                    <p className="text-white mb-0">Owner</p>
                    <p className="text-white">{owner_phonenumber}</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row mt-4">
              <div className="col-12">
                <p className='text-white'>
                  {description}
                </p>
              </div>
            </div>
            <div className="row mt-4">
              <div className="col-12">
                <Link to={'/comingsoon'}>
                <button className="btn btn-primary">
                  Schedule Viewing
                </button>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default PropertyDetailsRenter;
