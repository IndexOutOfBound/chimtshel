// Import necessary modules
const express = require('express');
const pg = require('pg');
const cors = require('cors');
const dotenv = require('dotenv');
const multer = require('multer');
const path = require('path');
// Load environment variables from .env file
dotenv.config();

// Create Express app
const app = express();

// Middleware
app.use(express.json());
app.use(cors());
app.use('/uploads', express.static('uploads'));
// Configure PostgreSQL connection
const pool = new pg.Pool({
  user: process.env.DB_USER,
  host: process.env.DB_HOST,
  database: process.env.DB_NAME,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT,
  ssl: true,
});

// Define API endpoint for user registration
app.post('/register', async (req, res) => {
    try {
      const { name, phoneNumber, email, password, gender, currentAddress, typeOfUser } = req.body;
  
      // Check if the email already exists in the database
      const client = await pool.connect();
      const emailCheckQuery = 'SELECT * FROM users WHERE email = $1';
      const emailCheckResult = await client.query(emailCheckQuery, [email]);
  
      if (emailCheckResult.rows.length > 0) {
        // If email already exists, return a 400 error
        client.release();
        return res.status(400).json({ message: 'Email already exists' });
      }
  
      // If email does not exist, insert the new user
      const insertionQuery = 'INSERT INTO users (name, phonenumber, email, password, gender, currentaddress, typeofuser) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING *';
      const result = await client.query(insertionQuery, [name, phoneNumber, email, password, gender, currentAddress, typeOfUser]);
      const newUser = result.rows[0];
      client.release();
      res.status(201).json({message:'Registrations successful'});
    } catch (error) {
      console.error('Error:', error);
      res.status(500).json({ message: 'Internal Server Error' });
    }
  });

  // Define API endpoint for user login
app.post('/login', async (req, res) => {
  try {
    const { email, password } = req.body;

    // Check if the email exists in the database
    const client = await pool.connect();
    const loginQuery = 'SELECT * FROM users WHERE email = $1 AND password = $2';
    const loginResult = await client.query(loginQuery, [email, password]);

    if (loginResult.rows.length === 0) {
      // If email and password don't match, return a 400 error
      client.release();
      return res.status(400).json({ message: 'Invalid email or password' });
    }

    const user = loginResult.rows[0];
    client.release();
    res.status(200).json({ role: user.typeofuser, name: user.name , email: user.email });
  } catch (error) {
    console.error('Error:', error);
    res.status(500).json({ message: 'Internal Server Error' });
  }
});

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/'); // Specify the destination folder
  },
  filename: function (req, file, cb) {
    // Use the original filename with a timestamp to avoid conflicts
    cb(null, Date.now() + path.extname(file.originalname));
  }
});
const upload = multer({ 
  storage: storage,
  limits: {
    fileSize: 10 * 1024 * 1024, // 10 MB file size limit
  }
}).array('images', 10);;



app.post('/owner/listproperties', upload, async (req, res) => {
  try {
    console.log('Req.Body', req.body);
    console.log('Req.Files', req.files); // Log uploaded files

    const { owner_email, propertyName, address, description, rent, imageDescriptions } = req.body;
    const images = req.files.map(file =>  file.filename); // Get paths of uploaded images
    console.log(images)
    // Insert property details into the properties table
    const propertyInsertQuery = `
      INSERT INTO properties (owner_email, property_name, address, description, rent, image_paths, image_descriptions)
      VALUES ($1, $2, $3, $4, $5, $6, $7)
      RETURNING *;
    `;
    const propertyValues = [owner_email, propertyName, address, description, rent, images, imageDescriptions];
    console.log('Property Values:', propertyValues); // Log property values

    const propertyResult = await pool.query(propertyInsertQuery, propertyValues);
    const insertedProperty = propertyResult.rows[0];

    res.status(201).json({ message: 'Property listed successfully', property: insertedProperty });
  } catch (error) {
    console.error('Error listing property:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// Fetch Listings Endpoint
app.get('/owner/listings', async (req, res) => {
  try {
    const owner_email = req.query.owner_email
    const listings = await pool.query('SELECT * FROM properties WHERE owner_email = $1', [owner_email]);
    res.json({ listings: listings.rows });
    
  } catch (error) {
    console.error('Error fetching listings:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// Deactivate Listing Endpoint
app.put('/owner/listings/:id/deactivate', async (req, res) => {
  const { id } = req.params;
  const { owner_email } = req.body;
  try {
    await pool.query('UPDATE properties SET is_active = FALSE WHERE property_id = $1 AND owner_email = $2', [id, owner_email]);
    res.json({ message: 'Listing deactivated successfully' });
  } catch (error) {
    console.error('Error deactivating listing:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// Delete Listing Endpoint
app.delete('/owner/listings/:id', async (req, res) => {
  const { id } = req.params;
  const { owner_email } = req.body;
  try {
    await pool.query('DELETE FROM properties WHERE property_id = $1 AND owner_email = $2', [id, owner_email]);
    res.json({ message: 'Listing deleted successfully' });
  } catch (error) {
    console.error('Error deleting listing:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

//View details owner
app.get('/properties/:propertyId', async (req, res) => {
  const { propertyId } = req.params;
  try {
    const property = await pool.query('SELECT * FROM properties WHERE property_id = $1', [propertyId]);
    if (property.rows.length === 0) {
      return res.status(404).json({ message: 'Property not found' });
    }
    res.json({ listing: property.rows[0] });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error fetching property details' });
  }
});

app.get('/renter/listings', async (req, res) => {
  try {
    const listingsQuery = 'SELECT * FROM properties WHERE is_active = TRUE';
    const listingsResult = await pool.query(listingsQuery);
    const listings = listingsResult.rows;

    // Get owner details for each listing
    for (const listing of listings) {
      const ownerQuery = 'SELECT name, phonenumber FROM users WHERE email = $1';
      const ownerResult = await pool.query(ownerQuery, [listing.owner_email]);
      const owner = ownerResult.rows[0];
      listing.owner_name = owner.name;
      listing.owner_phonenumber = owner.phonenumber;
    }

    res.json(listings);
  } catch (error) {
    console.error('Error fetching active listings:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

app.get('/renter/properties/:propertyId', async (req, res) => {
  const { propertyId } = req.params;
  console.log(req.params);
  try {
    const propertyQuery = 'SELECT * FROM properties WHERE property_id = $1 AND is_active = TRUE';
    const propertyResult = await pool.query(propertyQuery, [propertyId]);
    console.log(propertyResult);
    
    if (propertyResult.rows.length === 0) {
      return res.status(404).json({ message: 'Property not found or not active' });
    }
    
    const property = propertyResult.rows[0];
    
    const ownerQuery = 'SELECT name, phonenumber FROM users WHERE email = $1';
    const ownerResult = await pool.query(ownerQuery, [property.owner_email]);
    const owner = ownerResult.rows[0];
    
    property.owner_name = owner.name;
    property.owner_phonenumber = owner.phonenumber;
    
    res.json({ property });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Error fetching property details' });
  }
});
// Start server
const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
});
