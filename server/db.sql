CREATE TABLE "users" (
  "user_id" SERIAL PRIMARY KEY,
  "name" VARCHAR,
  "email" VARCHAR,
  "password" VARCHAR,
  "phonenumber" VARCHAR,
  "gender" VARCHAR,
  "currentaddress" VARCHAR,
  "typeofuser" VARCHAR
);

CREATE TABLE "properties" (
    "property_id" SERIAL PRIMARY KEY,
    "owner_email" VARCHAR NOT NULL, -- Foreign key referencing the owner table
    "property_name" VARCHAR(255) NOT NULL,
    "address" VARCHAR(255) NOT NULL,
    "description" TEXT,
    "rent" DECIMAL(10, 2) NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    -- Additional attributes for renter interaction
    "view_schedule" DATE, -- Date for property viewing schedule
    "is_active" BOOLEAN DEFAULT TRUE, -- Indicates if the property listing is active
    -- Attributes for storing images and descriptions
    "image_paths" TEXT[], -- Array to store paths of property images
    "image_descriptions" TEXT[] -- Array to store descriptions of property images
);